package com.example.rany.fragmentdemo.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rany.fragmentdemo.R;

public class FragmentOne extends Fragment{

    TextView tvTitle;
    private String title;

    public void setTitle(String title){
        this.title = title;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_one, container, false);
        tvTitle = view.findViewById(R.id.tvTitle);
        return view ;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTitle.setText(title);
    }

    private static FragmentOne INSTANCE;
    public static FragmentOne getINSTANCE(String tag){
        if(INSTANCE == null){
            INSTANCE = new FragmentOne();
        }
        Bundle b = new Bundle();
        b.putString("tag", tag);
        INSTANCE.setArguments(b);

        return INSTANCE;
    }
}
