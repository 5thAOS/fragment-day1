package com.example.rany.fragmentdemo;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.rany.fragmentdemo.fragment.FragmentOne;
import com.example.rany.fragmentdemo.fragment.FragmentTwo;

public class DynamicFragment extends AppCompatActivity {

    Button btnOne, btnTwo, btnRemove;
    public static final String FRAGMENT_TAG1 = "fragment1";
    public static final String FRAGMENT_TAG2 = "fragment2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_fragment);

        btnOne = findViewById(R.id.btnOne);
        btnTwo = findViewById(R.id.btnTwo);
        btnRemove = findViewById(R.id.btnRemove);

        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFragment(R.id.container, new FragmentOne(), FRAGMENT_TAG1,false);
            }
        });

        btnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(R.id.container, new FragmentTwo(), FRAGMENT_TAG2, true );
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFragment();
            }
        });
    }

    public void showFragment(int container, Fragment fragment, String tag, boolean addToBackStack){
        FragmentTransaction t = getFragmentManager().beginTransaction();
        t.add(container, fragment, tag);
        if(addToBackStack){
            t.addToBackStack(null);
        }
        t.commit();
    }

    public void replaceFragment(int container, Fragment fragment, String tag, boolean addToBackStack){
        FragmentTransaction t = getFragmentManager().beginTransaction();
        t.replace(container, fragment, tag);
        if(addToBackStack){
            t.addToBackStack(null);
        }
        t.commit();
    }

    public void removeFragment(){
        FragmentTransaction t = getFragmentManager().beginTransaction();
        FragmentTwo fragmentTwo = (FragmentTwo) getFragmentManager().findFragmentByTag(FRAGMENT_TAG2);
        if(fragmentTwo != null){
            t.remove(fragmentTwo);
            t.addToBackStack(null);
            t.commit();
        }

        else Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
    }


}
